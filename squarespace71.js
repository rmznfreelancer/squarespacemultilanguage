$(function() {
	
    /* SETUP MULTI-LANGUAGE */
    var defaultLanguage = 'tr';
    var lang = location.pathname.split("/")[1];
    var defaultClass = 'lang-'+defaultLanguage+'';
    var itemParent = "nav [class*='collection'],nav [class*='folder'],nav [class*='index'],nav [class*='group']";
    if (lang == "" || lang.length > 2 ){
    	var lang = defaultLanguage;
    }
    /* ADD LANGUAGE CLASSES */
    $('a[href="/"]').addClass('lang-'+defaultLanguage+'').parents(itemParent).addClass('lang-'+defaultLanguage+'');
    $('nav a:link:not([href^="http://"]):not([href^="https://"])').each(function () {
    	var langType = $(this).attr('href').split("/")[1];
     	var multiLanguageClass = 'multilanguage lang-' + langType + '';
      	if (undefined !== langType && langType.length <= 2) 
        	$(this).addClass(multiLanguageClass).parents(itemParent).addClass(multiLanguageClass);
    });
    $('nav button').each(function () {
      	var langTypeFolder = $(this).attr('data-controller-folder-toggle').split("/")[0];
      	var multiLanguageClass = 'multilanguage lang-' + langTypeFolder + '';
      	if (undefined !== langTypeFolder && langTypeFolder.length <= 2) 
        	$(this).addClass(multiLanguageClass);
    });
      
    /* ADD EXCLUSION NAV ITEMS */
    $('.exclude-me,.exclude-me a').addClass('exclude');
    $('.sqs-svg-icon--list a,.SocialLinks-link').addClass('exclude');

    /* REMOVE OTHER LANGUAGES AND KEEP EXCLUDED ITEMS */
    $('.multilanguage:not(".lang-'+lang+',.exclude")').remove();
  });